# -*- coding: utf-8 -*-
import torch
from torch.autograd import Variable
import numpy as np
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.neighbors import NearestNeighbors
from pyflann import *
import torch.nn.functional as F
from tqdm import tqdm
import torch.utils.data as data_utils
import torch.nn as nn

#plt.ion()
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import multiprocessing as mp
import pandas.io.parsers as pd
import time

from wxpy import *



def reduce_indices(coord,num):
	index = np.linspace(0,coord.shape[0],num, endpoint=False)
	index = index.astype(int)
	count = 0
	for i in index:
		if count == 0:
			target = coord[i,:]
			count = 1
		else:
			target = np.vstack((target,coord[i,:]))
	return target

def find_k_neighbors(coord,target,k):
	# ---- OLD KNN ----	

	dist = euclidean_distances(target,coord)
	indices = np.zeros((dist.shape[0],k))
	for i in range(dist.shape[0]):
		dist_arr = dist[i,:]
		indices[i,:] = np.argsort(dist_arr)[0:k]

	return indices

	# # return indices
	# print('compute distance')
	# nbrs = NearestNeighbors(n_neighbors=k, algorithm='ball_tree').fit(coord)
	# d, indices = nbrs.kneighbors(coord)
	# del d
	# return indices 


	# print('compute_distance')
	# flann = FLANN()
	# result, dists = flann.nn(
	#     coord, coord, k, algorithm="kmeans", branching=32, iterations=7, checks=16)
	# return result
      

def process_one_unit(index):
	#print index,hidden_layer
	k_nearest = k_nearest_list[hidden_layer]
	indices = Variable(torch.LongTensor([k for k in k_nearest[index,:]]))
	if run_on_gpu:
		indices = indices.cuda(0)
	x = torch.index_select(input_tensor, 1, indices)
	W = weight[hidden_layer]
	#print W.size()
	w = W[:,index]
	w = w[:,None] # Insert another dimension into the weights to permit multiplication
	y_pred_temp = x.mm(w)
	return y_pred_temp


def build_layer(input_tensor,k_nearest,num_units,weight,k,batch_size):

	# for i in range(num_units):
	# 	#print(i)
	# 	indices = Variable(torch.LongTensor([index for index in k_nearest[i,:]]))
	# 	x = torch.index_select(input_tensor, 1, indices)
	# 	w = weight[:,i]
	# 	w = w[:,None] # Insert another dimension into the weights to permit multiplication
	# 	y_pred_temp = x.mm(w)
	# 	if i == 0:
	# 		y_pred = y_pred_temp
	# 	else:
	# 		y_pred  = torch.cat((y_pred,y_pred_temp),1)
	# #print(type(y_pred))
	# return y_pred


	# # ---- Parallel Version -------
	# pool = mp.Pool(processes=4)
	# result = [pool.apply(process_one_unit, args=(x,)) for x in range(num_units)]
	# pool.close()
	# return torch.cat(result,1)




	# for i in range(64):
	# 	indices = Variable(torch.LongTensor([index for index in k_nearest[i,:]]))
	# 	inp = input_tensor[i,:]
	# 	print(inp.size)
	# 	inp = inp[None,:]
	#  	x_temp = torch.index_select(inp, 1, indices)
	#  	if i == 0:
	#  		x = x_temp
	#  	else:
	#  		x = torch.cat((x,x_temp),0)

	# return x.mm(weight).clamp(min=0)

	#input_tensor = input_tensor.data



	# --------------- ORIGINAL VERSION --------------------------
	k_nearest = Variable(torch.LongTensor(k_nearest))
	if run_on_gpu:
		k_nearest = k_nearest.cuda(0)

	diag_list = []

	for i in range(batch_size):

		x = input_tensor[i,:]
		x_temp = torch.take(x, k_nearest)
		out = x_temp* torch.t(weight)
		diag = torch.sum(out,1)
		diag_list.append(diag[:,None])

	output = torch.stack(diag_list,1).squeeze(2)
	output = torch.t(output)

	return output

	# ----------------------------------------------------






# class pruneNet(torch.nn.Module):
# 	def __init__(self, D_in, H, D_out):
# 	    super(DynamicNet, self).__init__()

# 	    self.linear = torch.nn.Linear(9, 1)
# 	    self.activation = nn.Relu()


#     def forward(self, x):

#     	dim = 1200

#     	H = []

#     	for i in range(dim):



def subtractMean(input):
	#print np.mean(input,axis = 0)
    meanValue = np.mean(input,axis = 0)
    #.savetxt("meanNeck.csv", meanValue, delimiter=",")

    input -= np.mean(input,axis = 0)
	#print np.mean(input,axis = 0)
    return input

# Nomralization by dividing the standard variation
def normalization(input):
    stdValue =np.std(input, axis = 0)
    #np.savetxt("stdNeck.csv", stdValue, delimiter=",")
    print np.std(input, axis = 0)
	#print np.std(input, axis = 0)
    input /= np.std(input, axis = 0)


	#print np.std(input, axis = 0)
    return input


class PruneNet(torch.nn.Module):
	def __init__(self, coord, D_in, D_out,k, number_hidden_layer, factor, batch_size, read_existing_weights,compute_k_nearest):
		super(PruneNet, self).__init__()
		self.dtype = torch.FloatTensor
		self.linear1 = torch.nn.Linear(k, 1)
		self.weight = nn.ParameterList()
		self.bias = nn.ParameterList()
		self.batch_size = batch_size
		self.k_nearest_list = []
		self.input_coord = coord
		self.number_hidden_layer = number_hidden_layer
		dim = D_in
		self.aux = []
		H_dim = []


		for i in range(number_hidden_layer):
			print i

			prev = dim
			dim = int(round(dim/factor))
			H_dim.append(dim)

			if read_existing_weights:
				weight.append(torch.load('prune_weights/%d.pt'%i).type(self.dtype))
				bias.append(torch.load('bias/%d.pt'%i))
				
			else:
				w = torch.IntTensor(k, dim).zero_().type(self.dtype)
				w = w.normal_(0,(2.0/dim)**0.5)
				p = torch.nn.Parameter(w.type(self.dtype),requires_grad=True)
				self.weight.append(p)
				b = torch.nn.Parameter(0 * torch.ones(self.batch_size,dim).type(self.dtype),requires_grad=True)
				self.bias.append(b)

			if compute_k_nearest:
				print "Compute KNN"
				target = reduce_indices(self.input_coord,dim)
				k_nearest = find_k_neighbors(self.input_coord,target,k)
				k_nearest = k_nearest.astype(int)

				a_list = [step * torch.ones(k_nearest.shape[0],k_nearest.shape[1]) for step in range(self.batch_size)]
				a = torch.stack(a_list,0)
				a = a * prev
				
				
				
				if run_on_gpu:
					k_nearest_v = Variable(torch.cuda.LongTensor(k_nearest))
					index = k_nearest_v.expand(self.batch_size,-1,-1)
					index = index + Variable((a.type(torch.cuda.LongTensor)))
				else:
					k_nearest_v = Variable(torch.LongTensor(k_nearest))
					index = k_nearest_v.expand(self.batch_size,-1,-1)
					index = index + Variable((a.type(torch.LongTensor)))
				self.aux.append(index)
			
				#k_nearest = np.random.randint(1, size=(dim,k))
				
				self.k_nearest_list.append(k_nearest)


				#np.savetxt('k_nearest/%d.txt'%i, k_nearest, delimiter=',')
				self.input_coord = target
				
			else:
				k_nearest = np.loadtxt('k_nearest/%d.txt'%i, delimiter=',')
				k_nearest = k_nearest.astype(int)
				k_nearest_list.append(k_nearest)

		if read_existing_weights:
			weight.append(torch.load('prune_weights/%d.pt'%(number_hidden_layer)).type(self.dtype))
			bias.append(torch.load('bias/%d.pt'%number_hidden_layer).type(self.dtype))
			#print(weight[-1].size())
		else:
			p = nn.Parameter(torch.randn(dim, D_out).type(self.dtype),requires_grad=True)
			self.weight.append(p)
			b = nn.Parameter(0.0 * torch.ones(self.batch_size,D_out).type(self.dtype),requires_grad=True)
			self.bias.append(b)


	def forward(self, input_tensor):
		
		for h in range(self.number_hidden_layer):
			
			diag_list = []
			

			# --- O ----
			# k_nearest = Variable(torch.LongTensor(self.k_nearest_list[h]))
			# k_nearest = k_nearest[:,:]
			# x_temp_list = []
			# for i in range(self.batch_size):
				
			#  	x = input_tensor[i,:]
			#  	x_temp = torch.take(x, k_nearest)
			#  	x_temp_list.append(x_temp)

			# x = torch.stack(x_temp_list,0)
			# ------

			
			x = torch.take(input_tensor,self.aux[h])

			# ------

			w = torch.t(self.weight[h])
			w = w.expand(self.batch_size,-1,-1)
			#print w.size()

			output = torch.mul(x,w)
			
			output = torch.sum(output,2)

			#output = F.tanh(output)+self.bias[h]
			output = F.relu(output)+self.bias[h]
			#print output.size()
			input_tensor = output





			
			# ------ Original --------------
			# tic = time.time()
			# for i in range(self.batch_size):
				
			# 	x = input_tensor[i,:]

				
			# 	x_temp = torch.take(x, k_nearest)

				

			# 	out = x_temp* torch.t(self.weight[h])
				
				
			# 	diag = torch.sum(out,1)
				
			# 	diag_list.append(diag[:,None])

			# #toc = time.time()
			# #print toc -tic, (toc-tic)/0.02 * 100


			# output = torch.stack(diag_list,1).squeeze(2)
			# output = torch.t(output)
			# output = F.tanh(output)+self.bias[h]

			# input_tensor = output
			# ----------------------------------

		y_pred = output.mm(self.weight[self.number_hidden_layer]) + self.bias[self.number_hidden_layer]
		
		return y_pred

def new_network(coord,k,factor,number_hidden_layer,read_existing_weights,compute_k_nearest):

	global input_tensor,output_tensor,k_nearest_list,hidden_layer,weight

	dtype = torch.cuda.FloatTensor if run_on_gpu else torch.FloatTensor


	N, D_in, D_out = 64*2000, coord.shape[0], 2
	#N_Validation = int(N/10)
	N_Validation = 64 * 200

	batch_size = 32


	print 'Loading data'
	tic = time.time()
	# x = np.genfromtxt('data/raytraceleft.csv',delimiter=',',max_rows = N+N_Validation)
	# #x = x[N:N+64*500,:]
	# y = np.genfromtxt('data/leftEye.csv',delimiter=',',max_rows = N+N_Validation)


	x = pd.read_csv('data/raytraceleft.csv',delimiter=',',nrows = N+N_Validation).values
	#x = x[N:N+64*500,:]
	y = pd.read_csv('data/leftEye.csv',delimiter=',',nrows = N+N_Validation).values

	toc = time.time()
	print 'Loading take %f seconds' %(toc - tic)

	



	validation_x = x[N:N+N_Validation,:]
	validation_y = y[N:N+N_Validation,:]

	x = x[0:N,:]
	y = y[0:N,:]

	#y = y[N:N+64*500,:]

	y = subtractMean(y)
	y = normalization(y)

	# x = subtractMean(x)
	# x = normalization(x)


	# x, y = torch.from_numpy(x), torch.from_numpy(y)

	# train = data_utils.TensorDataset(x, y)

	# train_loader = data_utils.DataLoader(train, batch_size=batch_size, shuffle=True)

	# ------ original ----
	x = Variable(torch.from_numpy(x).type(dtype),requires_grad=False)
	y = Variable(torch.from_numpy(y).type(dtype),requires_grad=False)


	# x = Variable(torch.randn(N, D_in).type(dtype), requires_grad=False)
	# y = Variable(torch.randn(N, D_out).type(dtype), requires_grad=False)


	validation_y = subtractMean(validation_y)
	validation_y = normalization(validation_y)
	
	

	validation_x = Variable(torch.from_numpy(validation_x).type(dtype),requires_grad=False)
	validation_y = Variable(torch.from_numpy(validation_y).type(dtype),requires_grad=False)

	iteration_number = 5000



	model = PruneNet(coord, D_in, D_out,k, number_hidden_layer, factor,batch_size, read_existing_weights,compute_k_nearest)
	if run_on_gpu:
		model.cuda()

	print model.parameters()
	optimizer = torch.optim.Adam(model.parameters(), lr = 1e-6)

	l = []
	validation_loss = []
	for t in range(iteration_number):


		# if t>120:
		# 	print "change lr"
		#  	optimizer = torch.optim.Adam(param, lr = 1e-5)


		# if t>2000:
		# 	print "change lr"
		#  	optimizer = torch.optim.Adam(param, lr = 1e-6)

		optimizer.zero_grad()
		total_loss = 0

		# for idx, (data, targets) in enumerate(train_loader):

		# 	if run_on_gpu:
		# 		data, targets = data.cuda(), targets.cuda()
		# 	input_tensor, output_tensor = Variable(data.type(dtype)), Variable(targets.type(dtype))


		# ----- ORIGINAL VERSION OF LOADING BATCHES --------------------------------------------

		for batch_num in tqdm(range(int((N/batch_size)))):
		#for batch_num in range(int((N/batch_size))):
			#print batch_num
			if (N - (batch_num+1)*batch_size)>=0:
				input_tensor = x[batch_num*batch_size:(batch_num+1)*batch_size,:]
				output_tensor = y[batch_num*batch_size:(batch_num+1)*batch_size,:]
				#print (batch_num+1)*batch_size
			else:
				break

			y_pred = model(input_tensor)

			#print toc -tic

			loss = (y_pred - output_tensor).pow(2).sum()

			loss.backward()

			toc = time.time()
			optimizer.step()
			#loss = torch.abs((y_pred - output_tensor)).sum()
			total_loss += loss.data[0]
		#print(t, loss.data[0])

		

		print(t,total_loss/N)
		l.append(total_loss/N)
		#writer.add_scalar('data/scalar', total_loss/(float(batch_num)*batch_size), t)

		# #plt.subplot(121)
		plt.plot(range(len(l)), np.asarray(l),'g')
		plt.pause(0.02)


		

		if t%10 == 0:
			# print "Save weights, bias and loss"
			# for k in range(len(weight)):
			# 	#print(weight[i].size())
			# 	torch.save(weight[k],'prune_weights/%d.pt'%k)
			# for b in range(len(bias)):
			# 	torch.save(bias[b],'bias/%d.pt'%b)
			np.savetxt('loss.txt',np.asarray(l),delimiter='\n')

			# Save model to disk
			torch.save(model.state_dict(), SAVE_MODEL_PATH)




		#optimizer.zero_grad()



		#Validation
		total_validation_loss = 0
		

		if t%10 == 0:

			for batch_num in range(int((N_Validation/batch_size))):
			#for batch_num in range(int((N/batch_size))):
				#print batch_num
				if (N_Validation - (batch_num+1)*batch_size)>=0:
					input_tensor = validation_x[batch_num*batch_size:(batch_num+1)*batch_size,:]
					output_tensor = validation_y[batch_num*batch_size:(batch_num+1)*batch_size,:]
					#print (batch_num+1)*batch_size
				else:
					break


				y_pred = model(input_tensor)
			

				#loss = (y_pred - output_tensor).pow(2).sum()
				loss = torch.abs((y_pred - output_tensor)).sum()

				total_validation_loss += loss.data[0]
				#print(t, loss.data[0])

			print "Validation loss"
			print(t,total_validation_loss/(float(batch_num)*batch_size))

			validation_loss.append(total_validation_loss/(float(batch_num)*batch_size))

			np.savetxt('val_loss.txt',np.asarray(validation_loss),delimiter='\n')

			


			#plt.subplot(122)

			plt.plot(range(0,t+10,10), np.asarray(validation_loss),'r')
			plt.pause(0.02)
			plt.savefig('./figure/loss_%d.png'%t)

			if enable_wechat_notification:
				target.send('Iter: %d \nLoss: %f \nVal Loss: %f' %(t,total_loss/N, total_validation_loss/(float(batch_num)*batch_size)))
				target.send_image('./figure/loss_%d.png'%t)






def network(coord,k,factor,number_hidden_layer,read_existing_weights,compute_k_nearest):

	global input_tensor,output_tensor,k_nearest_list,hidden_layer,weight

	dtype = torch.cuda.FloatTensor if run_on_gpu else torch.FloatTensor


	N, D_in, D_out = 64*50, coord.shape[0], 2
	#N_Validation = int(N/10)
	N_Validation = 10

	batch_size = 32


	print 'Loading data'
	x = np.genfromtxt('data/raytraceleft.csv',delimiter=',',max_rows = N+N_Validation)
	#x = x[N:N+64*500,:]
	y = np.genfromtxt('data/leftEye.csv',delimiter=',',max_rows = N+N_Validation)

	



	validation_x = x[N:N+N_Validation,:]
	validation_y = y[N:N+N_Validation,:]

	x = x[0:N,:]
	y = y[0:N,:]

	#y = y[N:N+64*500,:]

	y = subtractMean(y)
	y = normalization(y)

	# x = subtractMean(x)
	# x = normalization(x)


	# x, y = torch.from_numpy(x), torch.from_numpy(y)

	# train = data_utils.TensorDataset(x, y)

	# train_loader = data_utils.DataLoader(train, batch_size=batch_size, shuffle=True)

	# ------ original ----
	x = Variable(torch.from_numpy(x).type(dtype),requires_grad=False)
	y = Variable(torch.from_numpy(y).type(dtype),requires_grad=False)


	validation_y = subtractMean(validation_y)
	validation_y = normalization(validation_y)
	
	

	validation_x = Variable(torch.from_numpy(validation_x).type(dtype),requires_grad=False)
	validation_y = Variable(torch.from_numpy(validation_y).type(dtype),requires_grad=False)

	
	# x = Variable(torch.randn(N, D_in).type(dtype), requires_grad=False)
	# y = Variable(torch.randn(N, D_out).type(dtype), requires_grad=False)

	# x = np.random.randint(10, size=(N, D_in))
	# y = np.random.randint(10, size=(N, D_out))

	weight = []
	bias = []


	dim = D_in
	H_dim = []
	input_coord = coord
	k_nearest_list = []
	



	for i in range(number_hidden_layer):
		dim = int(round(dim/factor))
		H_dim.append(dim)

		if read_existing_weights:
			weight.append(torch.load('prune_weights/%d.pt'%i).type(dtype))
			bias.append(torch.load('bias/%d.pt'%i))
			# print(weight[i].size())
			
		else:
			# print dim
			#weight.append(Variable(torch.randn(k, dim).type(dtype), requires_grad=True))

			# He_normal
			w = torch.IntTensor(k, dim).zero_().type(dtype)
			#w = w.normal_(0,(2.0/dim)**0.5)
			w = w.normal_(0,0.2)
			weight.append(Variable(w.type(dtype),requires_grad=True))

			#weight.append(Variable(0.1 * torch.ones(k,dim),requires_grad=True))

			bias.append(Variable(0 * torch.ones(batch_size,dim).type(dtype),requires_grad=True))
		

		if compute_k_nearest:
			print "Compute KNN"
			target = reduce_indices(input_coord,dim)
			k_nearest = find_k_neighbors(input_coord,target,k)
			k_nearest = k_nearest.astype(int)
			#k_nearest = np.random.randint(1, size=(dim,k))
			k_nearest_list.append(k_nearest)
			np.savetxt('k_nearest/%d.txt'%i, k_nearest, delimiter=',')
			input_coord = target
		else:
			k_nearest = np.loadtxt('k_nearest/%d.txt'%i, delimiter=',')
			k_nearest = k_nearest.astype(int)
			k_nearest_list.append(k_nearest)


		


	# weight for output layer
	if read_existing_weights:
		weight.append(torch.load('prune_weights/%d.pt'%(number_hidden_layer)).type(dtype))
		bias.append(torch.load('bias/%d.pt'%number_hidden_layer).type(dtype))
		#print(weight[-1].size())
	else:
		weight.append(Variable(torch.randn(dim, D_out).type(dtype), requires_grad=True))
		#weight.append(Variable(0.1 * torch.ones(dim,D_out),requires_grad=True))
		bias.append(Variable(0.0 * torch.ones(batch_size,D_out).type(dtype),requires_grad=True))

	iteration_number = 5000
	learning_rate = 1e-4
	l = []


	param = weight+bias
	#print param

	optimizer = torch.optim.Adam(param, lr = 1e-4)

	#optimizer = torch.optim.SGD(param, lr = 0.1,momentum=0.9)
	
	

	print(H_dim)
	for t in range(iteration_number):

		# if t>120:
		# 	print "change lr"
		#  	optimizer = torch.optim.Adam(param, lr = 1e-5)


		# if t>2000:
		# 	print "change lr"
		#  	optimizer = torch.optim.Adam(param, lr = 1e-6)

		optimizer.zero_grad()
		total_loss = 0

		# for idx, (data, targets) in enumerate(train_loader):

		# 	if run_on_gpu:
		# 		data, targets = data.cuda(), targets.cuda()
		# 	input_tensor, output_tensor = Variable(data.type(dtype)), Variable(targets.type(dtype))


		# ----- ORIGINAL VERSION OF LOADING BATCHES --------------------------------------------

		for batch_num in tqdm(range(int((N/batch_size)))):
		#for batch_num in range(int((N/batch_size))):
			#print batch_num
			if (N - (batch_num+1)*batch_size)>=0:
				input_tensor = x[batch_num*batch_size:(batch_num+1)*batch_size,:]
				output_tensor = y[batch_num*batch_size:(batch_num+1)*batch_size,:]
				#print (batch_num+1)*batch_size
			else:
				break

			#print(weight[5])

			# mean =  torch.mean(output_tensor,1,True)
			# output_tensor = output_tensor - mean

			# std = torch.std(output_tensor,1,True)
			# output_tensor = output_tensor / std

		# -------------------------------------------------------------------------------------

			for i in range(number_hidden_layer):
				hidden_layer = i

				if i == 0:

					output = build_layer(input_tensor,k_nearest_list[i], H_dim[i],weight[i],k,batch_size)

				else:
					output = build_layer(output,k_nearest_list[i], H_dim[i],weight[i],k,batch_size)

				
				#
				output = F.tanh(output)+bias[i]

				
			# Build last layer

			y_pred = output.mm(weight[number_hidden_layer]) + bias[number_hidden_layer]
		

			loss = (y_pred - output_tensor).pow(2).sum()
			#loss = torch.abs((y_pred - output_tensor)).sum()

			total_loss += loss.data[0]
			#print(t, loss.data[0])

			loss.backward()
			optimizer.step()

		print(t,total_loss/N)
		l.append(total_loss/N)
		#writer.add_scalar('data/scalar', total_loss/(float(batch_num)*batch_size), t)

		plt.plot(np.asarray(l),'g')
		plt.pause(0.05)

		

		if t%10 == 0:
			print "Save weights, bias and loss"
			for k in range(len(weight)):
				#print(weight[i].size())
				torch.save(weight[k],'prune_weights/%d.pt'%k)
			for b in range(len(bias)):
				torch.save(bias[b],'bias/%d.pt'%b)
			np.savetxt('loss.txt',np.asarray(l),delimiter='\n')


		# --- Validation ------

		# optimizer.zero_grad()
		# total_validation_loss = 0
		# validation_loss = []

		# if t%10 == 0:

		# 	for batch_num in range(int((N_Validation/batch_size))):
		# 	#for batch_num in range(int((N/batch_size))):
		# 		#print batch_num
		# 		if (N_Validation - (batch_num+1)*batch_size)>=0:
		# 			input_tensor = validation_x[batch_num*batch_size:(batch_num+1)*batch_size,:]
		# 			output_tensor = validation_y[batch_num*batch_size:(batch_num+1)*batch_size,:]
		# 			#print (batch_num+1)*batch_size
		# 		else:
		# 			break


		# 		for i in range(number_hidden_layer):

		# 			if i == 0:
		# 				output = build_layer(input_tensor,k_nearest_list[i], H_dim[i],weight[i],k,batch_size)

		# 			else:
		# 				output = build_layer(output,k_nearest_list[i], H_dim[i],weight[i],k,batch_size)

		# 			output = F.relu(output)+bias[i]


		# 		y_pred = output.mm(weight[6]) + bias[6]
			

		# 		#loss = (y_pred - output_tensor).pow(2).sum()
		# 		loss = torch.abs((y_pred - output_tensor)).sum()

		# 		total_validation_loss += loss.data[0]
		# 		#print(t, loss.data[0])

		# 	print "Validation loss"
		# 	print(t,total_validation_loss/(float(batch_num)*batch_size))

		# 	validation_loss.append(total_validation_loss/(float(batch_num)*batch_size))

		# 	# plt.plot(np.asarray(validation_loss),'r')
		# 	# plt.pause(0.05)







#@profile

def test_model(coord,k,factor,number_hidden_layer,read_existing_weights):
	dtype = torch.FloatTensor

	N, D_in, D_out = 64, coord.shape[0], 2

	N_Validation = int(N/10)
	print 'Loading data'



	x = np.genfromtxt('data/raytraceleft.csv',delimiter=',',max_rows = N+N_Validation)
	#x = x[N:N+64*500,:]
	y = np.genfromtxt('data/leftEye.csv',delimiter=',',max_rows = N+N_Validation)
	batch_size = 64

	validation_x = x[N:N+N_Validation,:]
	validation_y = y[N:N+N_Validation,:]

	x = x[0:N,:]
	y = y[0:N,:]
	#y = y[N:N+64*500,:]
	
	x = Variable(torch.from_numpy(x).type(dtype),requires_grad=False)
	y = Variable(torch.from_numpy(y).type(dtype),requires_grad=False)

	print y

	# mean =  torch.mean(x,1,True)
	# x = x - mean

	# std = torch.std(x,1,True)
	# x = x / std


	# mean =  torch.mean(y,1,True)
	# y = y - mean

	# std = torch.std(y,1,True)
	# y = y / std



	H_dim = []
	prev = D_in
	dim = D_in
	weight = []
	bias = []
	for i in range(number_hidden_layer):
		dim = int(round(dim/factor))
		H_dim.append(dim)

		if read_existing_weights:
			weight.append(torch.load('prune_weights/%d.pt'%i).type(dtype))
			bias.append(torch.load('bias/%d.pt'%i))
			# print(weight[i].size())
			
		else:
			# print dim
			#weight.append(Variable(torch.randn(k, dim).type(dtype), requires_grad=True))

			# He_normal
			print dim
			w = torch.IntTensor(prev, dim).zero_().type(dtype)
			w = w.normal_(0,(2.0/dim)**0.5)
			print w.size()
			weight.append(Variable(w.type(dtype),requires_grad=True))

			#weight.append(Variable(0.1 * torch.ones(k,dim),requires_grad=True))

			bias.append(Variable(0.0 * torch.ones(batch_size,dim),requires_grad=True))
			prev = dim
		


	w = torch.IntTensor(prev, 2).zero_().type(dtype)
	w = w.normal_(0,(2.0/dim)**0.5)
	weight.append(Variable(w.type(dtype),requires_grad=True))

	bias.append(Variable(0.01 * torch.ones(batch_size,2),requires_grad=True))




	learning_rate = 1e-6

	param = weight + bias
	#print param

	optimizer = torch.optim.Adam(param, lr = 1e-6)

	for t in range(5000):
	    # Forward pass: compute predicted y using operations on Variables; these
	    # are exactly the same operations we used to compute the forward pass using
	    # Tensors, but we do not need to keep references to intermediate values since
	    # we are not implementing the backward pass by hand.
	    
	    

	    for i in range(number_hidden_layer):
	    	#print i
	    	#print weight[i].size()
	    	if i == 0:
	    		output = x.mm(weight[0])
	    	else:
	    		output = output.mm(weight[i])

	    	output = F.relu(output)+bias[i]
	    	#print output.size()

	    y_pred = output.mm(weight[6]) + bias[6]

	    loss = (y_pred - y).pow(2).sum()
	    #loss = torch.abs((y_pred - y)).sum()

	    print(t, loss.data[0]/64.0)

	    # Use autograd to compute the backward pass. This call will compute the
	    # gradient of loss with respect to all Variables with requires_grad=True.
	    # After this call w1.grad and w2.grad will be Variables holding the gradient
	    # of the loss with respect to w1 and w2 respectively.
	    optimizer.zero_grad()
	    loss.backward()
	    optimizer.step()






#@profile

def main():
	
	# dtype = torch.cuda.FloatTensor # Uncomment this to run on GPU

	# N is batch size; D_in is input dimension;
	# H is hidden dimension; D_out is output dimension.
	num = 3600

	temp_coord = np.loadtxt('test.csv',delimiter=',')
	print(temp_coord.shape)
	
	coord = temp_coord[0:num,:]
	del temp_coord
	print('Read coordinates')
	factor = 3
	k = 36
	number_hidden_layer = 6 
	save_model = True
	analyze_overlapping = False
	read_existing_weights = False
	compute_k_nearest = True

	global run_on_gpu, SAVE_MODEL_PATH, enable_wechat_notification
	enable_wechat_notification = True
	run_on_gpu = True


		
	SAVE_MODEL_PATH = './model/model'

	if enable_wechat_notification:
		
		bot = Bot(console_qr=True,cache_path=True)
		global target
		target = bot.self
		target.send('Starting New Training')
		target.send('k = %d \nfactor=%d \nn_layer = %d \n'%(k,factor,number_hidden_layer))

	new_network(coord,k,factor,number_hidden_layer,read_existing_weights,compute_k_nearest)
	#test_model(coord,k,factor,number_hidden_layer,read_existing_weights)



if __name__ == '__main__':
    main()


