import os
import struct
import numpy as np
import gzip
from tqdm import tqdm
import h5py
 
def load_mnist(path, kind='train'):
    """Load MNIST data from `path`"""
    labels_path = os.path.join(path, 
                               '%s-labels-idx1-ubyte.gz' % kind)
    images_path = os.path.join(path, 
                               '%s-images-idx3-ubyte.gz' % kind)
        
    with gzip.open(labels_path, 'rb') as lbpath:
        lbpath.read(8)
        buffer = lbpath.read()
        labels = np.frombuffer(buffer, dtype=np.uint8)

    with gzip.open(images_path, 'rb') as imgpath:
        imgpath.read(16)
        buffer = imgpath.read()
        images = np.frombuffer(buffer, 
                               dtype=np.uint8).reshape(
            len(labels), 784).astype(np.float64)
 
    return images, labels


def visualize_mnist():
    import matplotlib.pyplot as plt

    fig, ax = plt.subplots(nrows=2, ncols=5, sharex=True, sharey=True,)
    ax = ax.flatten()
    for i in range(10):
        img = X_train[y_train == i][0].reshape(28, 28)
        ax[i].imshow(img, cmap='Greys', interpolation='nearest')

    ax[0].set_xticks([])
    ax[0].set_yticks([])
    plt.tight_layout()
    # plt.savefig('./figures/mnist_all.png', dpi=300)
    plt.show()


import numpy as np;
import numpy.random as rd;
import matplotlib.image  as img;
import matplotlib.pyplot as plt;
from scipy.signal import convolve2d as conv2d;
def clip(n,N):
    if n <= 0:
        return 0;
    if n >= N-1:
        return N-1;
    return n;

def bilinear(I,x,y,bounds):
    x_min = int(np.floor(x));
    y_min = int(np.floor(y));
    
    x_max = 1 + x_min;
    y_max = 1 + y_min;
    
    u = x - x_min;
    v = y - y_min;
    
    x_min = bounds(x_min,I.shape[0]);
    x_max = bounds(x_max,I.shape[0]);
    y_min = bounds(y_min,I.shape[1]);
    y_max = bounds(y_max,I.shape[1]);
    
    f00 = I[x_min,y_min];
    f01 = I[x_min,y_max];
    f10 = I[x_max,y_min];
    f11 = I[x_max,y_max];
    return (1.0-u)*(1.0-v)*f00+u*(1.0-v)*f10+(1.0-u)*v*f01+u*v*f11;
'''
def to_polar(E,R,T,x0,y0,r_min,r_max,ipol,bounds):
    download_dir = "logpolar.csv" #where you want the file to be downloaded to

    csv = open(download_dir, "w")
    S = np.zeros((R,T)) if len(E.shape) == 2 else np.zeros((R,T,E.shape[2]));
    dr = (np.log(r_max)-np.log(r_min))/R;
    dt = 2.0*np.pi/T;
    for r in range(R):
        print(r_min*np.exp(dr*r))
        for t in range(T):
            tau = r_min*np.exp(dr*r);
            phi = dt*t;
            x = x0 + tau*np.cos(phi);
            y = y0 + tau*np.sin(phi);
          
            #if 0 <= x < E.shape[0] and 0 <= y < E.shape[1]:
            S[r,t] = ipol(E,x,y,bounds);
            if len(E.shape)==3:
                csv.write(str(ipol(E,x,y,bounds)[0])+',')
            else:
                csv.write(str(ipol(E,x,y,bounds))+',')
    return S;
'''

def to_polar(E,R,T,x0,y0,r_min,r_max,ipol,bounds, noise):
    #download_dir = "logpolar.csv" #where you want the file to be downloaded to
    output_arr = np.zeros((T*R,2))
    #csv = open(download_dir, "w")

    S = np.zeros((T,R)) if len(E.shape) == 2 else np.zeros((T,R,E.shape[2]));
    dr =R/5.0;
    dt = 2.0*np.pi/T;
    count = 0
    result = []
    for r in range(R):
        #print r_max*np.exp(r/5.0)/np.exp(R/5.0)
        for t in range(T):
            #tau = r_max*np.exp(r/5.0+ float(noise[R*r+t])*3)/np.exp(R/5.0)
            tau = r_max*np.exp(r/5.0)/np.exp(R/5.0)
            phi = dt*t
            #x = x0 + tau*np.cos(phi + float(noise[R*r+t]))
            x = x0 + tau*np.cos(phi)
            #y = y0 + tau*np.sin(phi + float(noise[R*r+t]))
            y = y0 + tau*np.sin(phi)
            
            output_arr[count,:] = [x,y]
            count = count + 1
            #if 0 <= x < E.shape[0] and 0 <= y < E.shape[1]:
            S[t,r] = ipol(E,x,y,bounds);
            if len(E.shape)==3:
                #csv.write(str(ipol(E,x,y,bounds)[0])+',')
                result.append(str(ipol(E,x,y,bounds)[0]))
            else:
                #csv.write(str(ipol(E,x,y,bounds))+',')
                result.append(str(ipol(E,x,y,bounds)))
    #np.savetxt('test.csv', output_arr, delimiter=',')

    return S, np.asarray(result);





# ========= Load Dataset ============================

X_train, y_train = load_mnist('data/', kind='train')
#print('Rows: %d, columns: %d' % (X_train.shape[0], X_train.shape[1]))

X_test, y_test = load_mnist('data/', kind='t10k')
#print('Rows: %d, columns: %d' % (X_test.shape[0], X_test.shape[1]))


# ========= Noise Log Polar Mapping =========


f = open('../retina_distribution_noise.txt', 'r')
noise =f.read().splitlines()
#print noise



# ====== Log Polar sampling for all the images =====
R = 40;
T = 90;

# Create the matrix to store the irriadance sampling

X_train_lp = np.zeros((X_train.shape[0],R*T)) 
X_test_lp = np.zeros((X_test.shape[0],R*T))


# Loop for the train dataset
for i in tqdm(range(60000)):
    #print i
    # ---- Code Segment to process one image ------
    #I = img.imread("h3.png");
    I = X_train[i,:].reshape(28, 28)
    
    if np.max(I)>1:
        I=I/255.0

    # Since all the pictures have the same dimension, we only need to do it once at i = 0
    if i == 0:
        x0 = 0.5+I.shape[0]/2;
        y0 = 0.5+I.shape[1]/2;

        r_min = 0.1;
        r_max = np.linalg.norm(np.array([x0,y0]))


    P, vector = to_polar(I,R,T,x0,y0,r_min,r_max,bilinear,clip,noise);

    X_train_lp[i,:] = vector

    # ------ End of code segment -----------------------------------
print X_train_lp
# Loop for the test_dataset

for i in tqdm(range(10000)):
    #print i
    # ---- Code Segment to process one image ------
    #I = img.imread("h3.png");
    I = X_test[i,:].reshape(28, 28)
    
    if np.max(I)>1:
        I=I/255.0

    # Since all the pictures have the same dimension, we only need to do it once at i = 0
    if i == 0:
        x0 = 0.5+I.shape[0]/2;
        y0 = 0.5+I.shape[1]/2;

        r_min = 0.1;
        r_max = np.linalg.norm(np.array([x0,y0]))


    P, vector = to_polar(I,R,T,x0,y0,r_min,r_max,bilinear,clip,noise);

    X_test_lp[i,:] = vector

    # ------ End of code segment -----------------------------------


h5f = h5py.File('./data/train_data.h5', 'w')
h5f.create_dataset('data', data=X_train_lp)
h5f.close()

h5f = h5py.File('./data/train_label.h5', 'w')
h5f.create_dataset('data', data=y_train)
h5f.close()

h5f = h5py.File('./data/test_data.h5', 'w')
h5f.create_dataset('data', data=X_test_lp)
h5f.close()

h5f = h5py.File('./data/test_label.h5', 'w')
h5f.create_dataset('data', data=y_test)
h5f.close()
# np.savetxt('./data/train_data.csv',X_train_lp,delimiter=',')
# np.savetxt('./data/train_label.csv',y_train,delimiter=',')

# np.savetxt('./data/test_data.csv',X_test_lp,delimiter=',')
# np.savetxt('./data/test_label.csv',y_test,delimiter=',')
