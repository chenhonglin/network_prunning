# -*- coding: utf-8 -*-
import torch
from torch.autograd import Variable
import numpy as np
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.neighbors import NearestNeighbors
from pyflann import *
import torch.nn.functional as F
from tqdm import tqdm
import torch.utils.data as data_utils

# import matplotlib.pyplot as plt
# import multiprocessing as mp

# plt.ion()


def reduce_indices(coord,num):
	index = np.linspace(0,coord.shape[0],num, endpoint=False)
	index = index.astype(int)
	count = 0
	for i in index:
		if count == 0:
			target = coord[i,:]
			count = 1
		else:
			target = np.vstack((target,coord[i,:]))
	return target

def find_k_neighbors(coord,target,k):
	# ---- OLD KNN ----	

	dist = euclidean_distances(target,coord)
	indices = np.zeros((dist.shape[0],k))
	for i in range(dist.shape[0]):
		dist_arr = dist[i,:]
		indices[i,:] = np.argsort(dist_arr)[0:k]

	return indices

	# # return indices
	# print('compute distance')
	# nbrs = NearestNeighbors(n_neighbors=k, algorithm='ball_tree').fit(coord)
	# d, indices = nbrs.kneighbors(coord)
	# del d
	# return indices 


	# print('compute_distance')
	# flann = FLANN()
	# result, dists = flann.nn(
	#     coord, coord, k, algorithm="kmeans", branching=32, iterations=7, checks=16)
	# return result
      

def process_one_unit(index):
	#print index,hidden_layer
	
	k_nearest = k_nearest_list[hidden_layer]
	indices = Variable(torch.LongTensor([k for k in k_nearest[index,:]]))
	indices = indices.cuda(0)
	x = torch.index_select(input_tensor, 1, indices)
	W = weight[hidden_layer]
	#print W.size()
	w = W[:,index]
	w = w[:,None] # Insert another dimension into the weights to permit multiplication
	y_pred_temp = x.mm(w)
	return y_pred_temp


def build_layer(input_tensor,k_nearest,num_units,weight,k,batch_size):

	# for i in range(num_units):
	# 	#print(i)
	# 	indices = Variable(torch.LongTensor([index for index in k_nearest[i,:]]))
	# 	x = torch.index_select(input_tensor, 1, indices)
	# 	w = weight[:,i]
	# 	w = w[:,None] # Insert another dimension into the weights to permit multiplication
	# 	y_pred_temp = x.mm(w)
	# 	if i == 0:
	# 		y_pred = y_pred_temp
	# 	else:
	# 		y_pred  = torch.cat((y_pred,y_pred_temp),1)
	# #print(type(y_pred))
	# return y_pred


	# # ---- Parallel Version -------
	# pool = mp.Pool(processes=4)
	# result = [pool.apply(process_one_unit, args=(x,)) for x in range(num_units)]
	# pool.close()
	# return torch.cat(result,1)




	# for i in range(64):
	# 	indices = Variable(torch.LongTensor([index for index in k_nearest[i,:]]))
	# 	inp = input_tensor[i,:]
	# 	print(inp.size)
	# 	inp = inp[None,:]
	#  	x_temp = torch.index_select(inp, 1, indices)
	#  	if i == 0:
	#  		x = x_temp
	#  	else:
	#  		x = torch.cat((x,x_temp),0)

	# return x.mm(weight).clamp(min=0)

	#input_tensor = input_tensor.data



	# --------------- ORIGINAL VERSION --------------------------
	k_nearest = Variable(torch.LongTensor(k_nearest))
	k_nearest = k_nearest.cuda(0)
	diag_list = []

	for i in range(batch_size):
		#n_temp = np.take(input_tensor[i,:],k_nearest)
		#x_temp = Variable (torch.FloatTensor(n_temp))
	
		x = input_tensor[i,:]
		x_temp = torch.take(x, k_nearest)



		w_t = torch.t(weight)
		out = x_temp*w_t
		diag = torch.sum(out,1)
		#diag = diag.clamp(min=0)
		diag_list.append(diag[:,None])

		del out

		# if i == 0:
	 #  		output = diag

	 #  	else:
	 #  		output = torch.cat((output,diag),1)

	output = torch.stack(diag_list,1).squeeze(2)
	output = torch.t(output)

	return output

	# ----------------------------------------------------



# class pruneNet(torch.nn.Module):
# 	def __init__(self, D_in, H, D_out):
# 	    super(DynamicNet, self).__init__()

# 	    self.linear = torch.nn.Linear(9, 1)
# 	    self.activation = nn.Relu()


#     def forward(self, x):

#     	dim = 1200

#     	H = []

#     	for i in range(dim):



def subtractMean(input):
	#print np.mean(input,axis = 0)
    meanValue = np.mean(input,axis = 0)
    #.savetxt("meanNeck.csv", meanValue, delimiter=",")
    input -= np.mean(input,axis = 0)
	#print np.mean(input,axis = 0)
    return input

# Nomralization by dividing the standard variation
def normalization(input):
    stdValue =np.std(input, axis = 0)
    #np.savetxt("stdNeck.csv", stdValue, delimiter=",")

	#print np.std(input, axis = 0)
    input /= np.std(input, axis = 0)
	#print np.std(input, axis = 0)
    return input



def network(coord,k,factor,number_hidden_layer,read_existing_weights,compute_k_nearest):

	global input_tensor,output_tensor,k_nearest_list,hidden_layer,weight,dtype


 	#dtype = torch.FloatTensor

	dtype = torch.cuda.FloatTensor # Uncomment this to run on GPU
	
	N, D_in, D_out = 64*100, coord.shape[0], 2
	N_Validation = int(N/10)
	print 'Loading data'
	x = np.genfromtxt('data/raytraceleft.csv',delimiter=',',max_rows = N+N_Validation)
	#x = x[N:N+64*500,:]
	y = np.genfromtxt('data/leftEye.csv',delimiter=',',max_rows = N+N_Validation)

	


	validation_x = x[N:N+N_Validation,:]
	validation_y = y[N:N+N_Validation,:]

	x = x[0:N,:]
	y = y[0:N,:]
	#y = y[N:N+64*500,:]




	y = subtractMean(y)
	y = normalization(y)

	validation_y = subtractMean(validation_y)
	validation_y = normalization(validation_y)
	
	#x = Variable(torch.from_numpy(x).type(dtype),requires_grad=False)
	#y = Variable(torch.from_numpy(y).type(dtype),requires_grad=False)

	x, y = torch.from_numpy(x), torch.from_numpy(y)

	train = data_utils.TensorDataset(x, y)

	train_loader = data_utils.DataLoader(train, batch_size=batch_size, shuffle=True)


	validation_x = Variable(torch.from_numpy(validation_x).type(dtype),requires_grad=False)
	validation_y = Variable(torch.from_numpy(validation_y).type(dtype),requires_grad=False)


	weight = []
	bias = []


	dim = D_in
	H_dim = []
	input_coord = coord
	k_nearest_list = []
	batch_size = 32



	for i in range(number_hidden_layer):
		dim = int(round(dim/factor))
		H_dim.append(dim)

		if read_existing_weights:
			weight.append(torch.load('prune_weights/%d.pt'%i).type(dtype))
			bias.append(torch.load('bias/%d.pt'%i))
			# print(weight[i].size())
			
		else:
			# print dim
			#weight.append(Variable(torch.randn(k, dim).type(dtype), requires_grad=True))

			# He_normal
			w = torch.IntTensor(k, dim).zero_().type(dtype)
			w = w.normal_(0,(2.0/dim)**0.5)
			weight.append(Variable(w.type(dtype),requires_grad=True))

			#weight.append(Variable(0.1 * torch.ones(k,dim),requires_grad=True))

			bias.append(Variable(0 * torch.ones(batch_size,dim).type(dtype),requires_grad=True))
		

		if compute_k_nearest:
			print "Compute KNN"
			target = reduce_indices(input_coord,dim)
			k_nearest = find_k_neighbors(input_coord,target,k)
			k_nearest = k_nearest.astype(int)
			#k_nearest = np.random.randint(1, size=(dim,k))
			k_nearest_list.append(k_nearest)
			np.savetxt('k_nearest/%d.txt'%i, k_nearest, delimiter=',')
			input_coord = target
		else:
			k_nearest = np.loadtxt('k_nearest/%d.txt'%i, delimiter=',')
			k_nearest = k_nearest.astype(int)
			k_nearest_list.append(k_nearest)


		


	# weight for output layer
	if read_existing_weights:
		weight.append(torch.load('prune_weights/%d.pt'%(number_hidden_layer)).type(dtype))
		bias.append(torch.load('bias/%d.pt'%number_hidden_layer))
		#print(weight[-1].size())
	else:
		weight.append(Variable(torch.randn(dim, D_out).type(dtype), requires_grad=True))
		#weight.append(Variable(0.1 * torch.ones(dim,D_out),requires_grad=True))
		bias.append(Variable(0.0 * torch.ones(batch_size,D_out).type(dtype),requires_grad=True))

	iteration_number = 4000
	learning_rate = 1e-4
	l = []


	param = weight+bias
	#print param

	optimizer = torch.optim.Adam(param, lr = 1e-5)

	#optimizer = torch.optim.SGD(param, lr = 0.1,momentum=0.9)
	
	

	print(H_dim)
	for t in range(iteration_number):

		# if t>30:
		#  	optimizer = torch.optim.Adam(param, lr = 1e-6)

		optimizer.zero_grad()
		total_loss = 0

		for idx, (data, targets) in enumerate(train_loader):

			data, targets = data.cuda(), targets.cuda()
			input_tensor, output_tensor = Variable(data), Variable(targets)


		# ----- Original -------

		# for batch_num in tqdm(range(int((N/batch_size)))):
		# #for batch_num in range(int((N/batch_size))):
		# 	#print batch_num
		# 	if (N - (batch_num+1)*batch_size)>=0:
		# 		input_tensor = x[batch_num*batch_size:(batch_num+1)*batch_size,:]
		# 		output_tensor = y[batch_num*batch_size:(batch_num+1)*batch_size,:]
		# 		#print (batch_num+1)*batch_size
		# 	else:
		# 		break

		# 	#print(weight[5])

		# 	# mean =  torch.mean(output_tensor,1,True)
		# 	# output_tensor = output_tensor - mean

		# 	# std = torch.std(output_tensor,1,True)
		# 	# output_tensor = output_tensor / std

		# ---------


			for i in range(number_hidden_layer):
				hidden_layer = i

				if i == 0:

					output = build_layer(input_tensor,k_nearest_list[i], H_dim[i],weight[i],k,batch_size)

				else:
					output = build_layer(output,k_nearest_list[i], H_dim[i],weight[i],k,batch_size)

				output = F.relu(output)+bias[i]

				# if i == 5:
				# 	print output


			# Build last layer

			y_pred = output.mm(weight[6]) + bias[6]
		

			loss = (y_pred - output_tensor).pow(2).sum()
			#loss = torch.abs((y_pred - output_tensor)).sum()

			total_loss += loss.data[0]
			#print(t, loss.data[0])

			loss.backward()
			optimizer.step()

			#print bias[5]
			#print(optimizer.state_dict()['state'])

			
			# for i in range(len(weight)):
			# 	#print(weight[i].grad.data)
			# 	#weight[i].data = weight[i].data - learning_rate * weight[i].grad.data

			# 	#if i == 6:
			# 		#print weight[i].grad.data

			# 	weight[i].data -= learning_rate * weight[i].grad.data
			# 	bias[i].data -= learning_rate * bias[i].grad.data

			# 	weight[i].grad.data.zero_()
			# 	bias[i].grad.data.zero_()

			# for k in range(len(weight)):
			# 	print'11'
			# 	print(weight[i].size())

		print(t,total_loss/(float(batch_num+1)*batch_size))
		l.append(total_loss/(float(batch_num+1)*batch_size))
		#writer.add_scalar('data/scalar', total_loss/(float(batch_num)*batch_size), t)

		# plt.plot(np.asarray(l),'g')
		# plt.pause(0.05)

		

		if t%10 == 0:
			print "Save weights, bias and loss"
			for k in range(len(weight)):
				#print(weight[i].size())
				torch.save(weight[k],'prune_weights/%d.pt'%k)
			for b in range(len(bias)):
				torch.save(bias[b],'bias/%d.pt'%b)
			np.savetxt('loss.txt',np.asarray(l),delimiter='\n')


		# --- Validation ------

		optimizer.zero_grad()
		total_validation_loss = 0
		validation_loss = []

		if t%10 == 0:

			for batch_num in range(int((N_Validation/batch_size))):
			#for batch_num in range(int((N/batch_size))):
				#print batch_num
				if (N_Validation - (batch_num+1)*batch_size)>=0:
					input_tensor = validation_x[batch_num*batch_size:(batch_num+1)*batch_size,:]
					output_tensor = validation_y[batch_num*batch_size:(batch_num+1)*batch_size,:]
					#print (batch_num+1)*batch_size
				else:
					break


				for i in range(number_hidden_layer):

					if i == 0:
						output = build_layer(input_tensor,k_nearest_list[i], H_dim[i],weight[i],k,batch_size)

					else:
						output = build_layer(output,k_nearest_list[i], H_dim[i],weight[i],k,batch_size)

					output = F.relu(output)+bias[i]


				y_pred = output.mm(weight[6]) + bias[6]
			

				#loss = (y_pred - output_tensor).pow(2).sum()
				loss = torch.abs((y_pred - output_tensor)).sum()

				total_validation_loss += loss.data[0]
				#print(t, loss.data[0])

			print "Validation loss"
			print(t,total_validation_loss/(float(batch_num)*batch_size))

			validation_loss.append(total_validation_loss/(float(batch_num)*batch_size))

			# plt.plot(np.asarray(validation_loss),'r')
			# plt.pause(0.05)







#@profile

def test_model(coord,k,factor,number_hidden_layer,read_existing_weights):
	dtype = torch.FloatTensor

	N, D_in, D_out = 64, coord.shape[0], 2

	N_Validation = int(N/10)
	print 'Loading data'



	x = np.genfromtxt('data/raytraceleft.csv',delimiter=',',max_rows = N+N_Validation)
	#x = x[N:N+64*500,:]
	y = np.genfromtxt('data/leftEye.csv',delimiter=',',max_rows = N+N_Validation)
	batch_size = 64

	validation_x = x[N:N+N_Validation,:]
	validation_y = y[N:N+N_Validation,:]

	x = x[0:N,:]
	y = y[0:N,:]
	#y = y[N:N+64*500,:]
	
	x = Variable(torch.from_numpy(x).type(dtype),requires_grad=False)
	y = Variable(torch.from_numpy(y).type(dtype),requires_grad=False)

	print y

	# mean =  torch.mean(x,1,True)
	# x = x - mean

	# std = torch.std(x,1,True)
	# x = x / std


	# mean =  torch.mean(y,1,True)
	# y = y - mean

	# std = torch.std(y,1,True)
	# y = y / std



	H_dim = []
	prev = D_in
	dim = D_in
	weight = []
	bias = []
	for i in range(number_hidden_layer):
		dim = int(round(dim/factor))
		H_dim.append(dim)

		if read_existing_weights:
			weight.append(torch.load('prune_weights/%d.pt'%i).type(dtype))
			bias.append(torch.load('bias/%d.pt'%i))
			# print(weight[i].size())
			
		else:
			# print dim
			#weight.append(Variable(torch.randn(k, dim).type(dtype), requires_grad=True))

			# He_normal
			print dim
			w = torch.IntTensor(prev, dim).zero_().type(dtype)
			w = w.normal_(0,(2.0/dim)**0.5)
			print w.size()
			weight.append(Variable(w.type(dtype),requires_grad=True))

			#weight.append(Variable(0.1 * torch.ones(k,dim),requires_grad=True))

			bias.append(Variable(0.0 * torch.ones(batch_size,dim),requires_grad=True))
			prev = dim
		


	w = torch.IntTensor(prev, 2).zero_().type(dtype)
	w = w.normal_(0,(2.0/dim)**0.5)
	weight.append(Variable(w.type(dtype),requires_grad=True))

	bias.append(Variable(0.01 * torch.ones(batch_size,2),requires_grad=True))




	learning_rate = 1e-6

	param = weight + bias
	#print param

	optimizer = torch.optim.Adam(param, lr = 1e-6)

	for t in range(5000):
	    # Forward pass: compute predicted y using operations on Variables; these
	    # are exactly the same operations we used to compute the forward pass using
	    # Tensors, but we do not need to keep references to intermediate values since
	    # we are not implementing the backward pass by hand.
	    
	    

	    for i in range(number_hidden_layer):
	    	#print i
	    	#print weight[i].size()
	    	if i == 0:
	    		output = x.mm(weight[0])
	    	else:
	    		output = output.mm(weight[i])

	    	output = F.relu(output)+bias[i]
	    	#print output.size()

	    y_pred = output.mm(weight[6]) + bias[6]

	    loss = (y_pred - y).pow(2).sum()
	    #loss = torch.abs((y_pred - y)).sum()

	    print(t, loss.data[0]/64.0)

	    # Use autograd to compute the backward pass. This call will compute the
	    # gradient of loss with respect to all Variables with requires_grad=True.
	    # After this call w1.grad and w2.grad will be Variables holding the gradient
	    # of the loss with respect to w1 and w2 respectively.
	    optimizer.zero_grad()
	    loss.backward()
	    optimizer.step()






#@profile

def main():
	
	# dtype = torch.cuda.FloatTensor # Uncomment this to run on GPU

	# N is batch size; D_in is input dimension;
	# H is hidden dimension; D_out is output dimension.
	num = 3600

	temp_coord = np.loadtxt('test.csv',delimiter=',')
	print(temp_coord.shape)
	
	coord = temp_coord[0:num,:]
	del temp_coord
	print('Read coordinates')
	factor = 3 
	k =9 
	number_hidden_layer =6 
	save_model = False
	analyze_overlapping = False
	read_existing_weights = False
	compute_k_nearest = True

	network(coord,k,factor,number_hidden_layer,read_existing_weights,compute_k_nearest)
	#test_model(coord,k,factor,number_hidden_layer,read_existing_weights)



if __name__ == '__main__':
    main()


