
import numpy as np 
from sklearn.metrics.pairwise import euclidean_distances
from keras.models import Model
from keras.layers import Dense, Input, Concatenate, Lambda,Activation
from keras.utils import plot_model
from tqdm import tqdm
import keras.backend as K
from keras.models import model_from_json


def reduce_indices(coord,num):
	index = np.linspace(0,coord.shape[0],num, endpoint=False)
	index = index.astype(int)
	count = 0
	for i in index:
		if count == 0:
			target = coord[i,:]
			count = 1
		else:
			target = np.vstack((target,coord[i,:]))
	return target

def find_k_neighbors(coord,target,k):	
	dist = euclidean_distances(target,coord)
	indices = np.zeros((dist.shape[0],k))
	for i in range(dist.shape[0]):
		dist_arr = dist[i,:]
		indices[i,:] = np.argsort(dist_arr)[0:k]

	return indices

def construct_model(factor,input_dim,save_model):
	from keras.layers import Dense
	inputTensor = Input((input_dim,))
	# outputTensor = Dense(1200)(inputTensor)
	# outputTensor = Dense(400)(outputTensor)
	# outputTensor = Dense(133)(outputTensor)
	# outputTensor = Dense(44)(outputTensor)
	# outputTensor = Dense(15)(outputTensor)
	# outputTensor = Dense(5)(outputTensor)

	outputTensor = Dense(12)(inputTensor)
	outputTensor = Dense(4)(outputTensor)


	outputTensor = Dense(2)(outputTensor)
	model = Model(inputTensor,outputTensor)
	model.compile(loss='mae', optimizer='sgd', metrics=['accuracy'])

	if save_model:
		model.save('dense_model.h5')  # creates a HDF5 file 'my_model.h5'
		#del model  # deletes the existing model

	return model

def build_hidden_layer(inputTensor,input_coord,num,k):
	groups = []

	target = reduce_indices(input_coord,num)
	k_nearest = find_k_neighbors(input_coord,target,k)
	k_nearest = k_nearest.astype(int)

	for i in tqdm(range(k_nearest.shape[0])):
		#print(inputTensor.op)
		grp = Lambda(lambda x: K.gather(K.transpose(x),k_nearest[i,:].tolist()))(inputTensor)
		grp = Lambda(lambda x: K.transpose(x))(grp)
		grp = Dense(1)(grp)
		groups.append(grp)

	my_concat = Lambda(lambda x: K.concatenate([x[i] for i in range(k_nearest.shape[0])],axis=-1))
	outputTensor = my_concat((groups))
	return outputTensor,target


def construct_prune_model(factor,k,input_dim,coord,number_hidden_layer,save_model):
	inputTensor = Input((input_dim,))

	print('Hidden Layer 1')
	dim = round(input_dim/factor)
	outputTensor,coord_for_next_layer = build_hidden_layer(inputTensor,coord,dim,k)

	for i in range(number_hidden_layer - 1):
		print('Hidden Layer %d' %(i+2))
		dim = round(dim / factor)
		outputTensor,coord_for_next_layer = build_hidden_layer(outputTensor,coord_for_next_layer,dim,k)

	print('Output Layer')
	outputTensor = Dense(2)(outputTensor)
	#create the model:
	model = Model(inputTensor,outputTensor)
	model.compile(loss='mae', optimizer='sgd', metrics=['accuracy'])
	print('Done')
	
	if save_model:
		model.save('prune_model.h5')  # creates a HDF5 file 'my_model.h5'
		# deletes the existing model
	return model

def construct_example_tensor():
	inputTensor = Input((8,))

	group1 = Lambda(lambda x: K.gather(K.transpose(x),[1,2,3]))(inputTensor)
	group2 = Lambda(lambda x: K.gather(K.transpose(x),[1,2,3]))(inputTensor)
	group3 = Lambda(lambda x: K.gather(K.transpose(x),[1,2,3]))(inputTensor)
	group4 = Lambda(lambda x: K.gather(K.transpose(x),[1,2,3]))(inputTensor)

	group1 = Lambda(lambda x: K.transpose(x))(group1)
	group2 = Lambda(lambda x: K.transpose(x))(group2)

	group1 = Dense(1)(group1)
	group2 = Dense(1)(group2)
	#group3 = Dense(1)(group3)   
	#group4 = Dense(1)(group4)

	my_concat = Lambda(lambda x: K.concatenate([x[0],x[1]],axis=-1))
	output = my_concat([group1,group2])
	# outputTensor = Concatenate()([group1,group2,group3,group4])
	# outputTensor = Activation('tanh')(outputTensor)
	
	# outputTensor = Dense(2)(outputTensor)
	# outputTensor = Activation('linear')(outputTensor)
	model = Model(inputTensor,output)

	# #create the model:
	# model = Model(inputTensor,outputTensor)
	return model


@profile

def main():
	coord = np.loadtxt('test.csv',delimiter=',')

	factor = 3
	k = 9
	input_dim = 36
	coord = coord[0:input_dim]
	number_hidden_layer = 2
	save_model = True
	analyze_overlapping = False
	

	# -------- Construct Model ------------------
	model = construct_model(factor,input_dim,save_model)
	#model = construct_prune_model(factor,k,input_dim,coord,number_hidden_layer,save_model)
	arr = np.arange(38*3)
	arr = arr.reshape((3,38))
	print('Train')

	model.fit(arr[:,0:36],arr[:,36:38],epochs=10)


	


	# -------------------------------------------

	# ------ Plot Model ----------------
	#plot_model(model, to_file='model.png')
	# ------------------------------------


	# ------- Analyze Overlapping -----------

	if analyze_overlapping:
		target = reduce_indices(coord,input_dim/factor)
		k_nearest = find_k_neighbors(coord,target,k)
		k_nearest = k_nearest.astype(int)

		count = np.zeros((3600,1))

		for i in range(k_nearest.shape[0]):
			for j in range(k_nearest.shape[1]):
				count[k_nearest[i,j]] += 1


		count_one = 0
		for i in range(k_nearest.shape[0]):
			if count[i] == 1:
				print('[Index:%d]--------------------'%i)
				count_one += 1
			else:
				print(count[i])

		print(count_one)


	# -------------------------------


if __name__ == '__main__':
    main()



